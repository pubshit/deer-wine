import React from 'react'
import NavBar from "../components/NavBar";
import Announcement from "../components/Announcement";
import Slider from "../components/Slider";
import Category from "../components/Category";

function Home() {
  return (
	<div>
	  <NavBar/>
	  <Announcement/>
	  <Slider/>
	  <Category/>
	</div>
  )
}

export default Home




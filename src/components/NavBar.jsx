import React from 'react'
import Badge from '@mui/material/Badge';
import {styled} from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import {FaOpencart} from 'react-icons/fa';


const StyledBadge = styled(Badge)(({theme}) => ({
  '& .MuiBadge-badge': {
	right: -5,
	top: 23,
	border: `2px solid ${theme.palette.background.paper}`,
	padding: '0 4px',
  },
}));

function NavBar() {
  return (
	<div className="nav__container grid">
	  <div className="nav-left position-relative">
        <span>
            <input
			  className="skinny"
			  id="english"
			  type="text"
			  placeholder="Look for your flavour?"
			/>
            <label className=" flex flex-middle flex-center">
                EN
            </label>
        </span>
      </div>
	  <div className="nav-center">
		Deer Wine
	  </div>
	  <div className="nav-right grid">
		<div></div>
		<div>Register</div>
		<div>login</div>
		<div className="position-relative">
		  <IconButton className=" left-0 position-absolute ">
			<StyledBadge badgeContent={4} color="primary">
			  <FaOpencart/>
			</StyledBadge>
		  </IconButton>
		</div>
	  </div>
	</div>
  )
}

export default NavBar

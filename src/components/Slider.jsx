import React, {useState} from "react";
import {AiOutlineSwapLeft, AiOutlineSwapRight} from "react-icons/ai";
import IconButton from "@mui/material/IconButton";
import {wineSlide} from "../resources/resourcesData";

const CARDS = wineSlide
const MAX_VISIBILITY = 1;

function Sliders() {
  const [active, setActive] = useState(1);
  const count = CARDS.length;
  
  const handleActive = (source) => {
	let limitLeft = 0;
	let limitRight = count - 1;
	if (active === limitLeft && source === 'left') {
	  setActive(limitRight);
	  return;
	}
	if (active === limitRight && source === 'right') {
	  setActive(limitLeft);
	  return;
	}
	source === 'left' ? setActive(active - 1) : setActive(active + 1);
  }
  
  return (
	<div className="slider position-relative">
	  <div className="nav left">
		<IconButton name="left" onClick={() => handleActive('left')}>
		  <AiOutlineSwapLeft name="name" size={45} color="#fff"/>
		</IconButton>
	  </div>
	  <div className="nav right">
		<IconButton onClick={() => handleActive('right')}>
		  <AiOutlineSwapRight size={45} color="#fff"/>
		</IconButton>
	  </div>
	  <div className="carousel flex flex-middle">
		{CARDS.map((item, i) => (
		  <div
			key={item.id}
			className="card__container"
			style={{
			  "--active": i === active ? 1 : 0,
			  "--offset": (active - i) / 3,
			  "--direction": Math.sign(active - i),
			  "--abs-offset": Math.abs(active - i) / 3,
			  // "pointer-events": active === i ? "auto" : "none",
			  opacity: Math.abs(active - i) >= MAX_VISIBILITY ? "0" : "1",
			  display: Math.abs(active - i) > MAX_VISIBILITY ? "none" : "block"
			}}
		  >
			<div className="card flex-column flex-middle flex-center ">
			  <div className="img__container-slide ">
				<img src={item.url} alt="slide product"/>
			  </div>
			  {/*<span style={{backgroundImage:`url('${item}')`}}>{item.name}</span>*/}
			  <span>{item.name}</span>
			  <p>
				{item.description}
			  </p>
			  <div className="slide-fade flex">
				<span>
				  <a href="?">Shop now</a>
				</span>
			  </div>
			</div>
		  </div>
		))}
	  
	  </div>
	</div>
  )
}

export default Sliders





import React, {useState} from 'react'
import {categories, nationalities} from "../resources/resourcesData";
import {Card, CardContent, Container, Grid, Collapse} from "@mui/material";

function Category() {
  const [collapse, setCollapse] = useState({
		nationalities: false,
		wineType: false,
	});
  
  const imagesCategory = {};
  let categoryImages = require.context('../../public/assets/images/', false, /\.(png|jpe?g|svg)$/)
  categoryImages.keys().forEach((key) => (imagesCategory[key] = categoryImages(key)));
  
	const handleCollapse = (e) => {
		setCollapse({
			...collapse, [e.target.id]: !collapse[e.target.id]
		})
	}

  return (
	<div className="grid category-container h-700  mt-16">
	  <div className="menu-category unSelected-paragraph">
			<h1 className='text-center'>Category</h1>
			{/* Lọc theo quốc gia */}
			<div
				id="nationalities"
			 	onClick={handleCollapse} 
			 	className='nationalities flex'
			 >
				Nation
			</div>
			<Collapse in={collapse.nationalities} component="ul">
				{nationalities?.map(nation => <li className='unSelected-paragraph'>{nation.name}</li>)}
			</Collapse>

			{/* Lọc theo thể loại rượu */}
			<div 
				id="wineType"
			 	onClick={handleCollapse} 
			 	className='nationalities flex'
			>
				Type of Wine
			</div>
			<Collapse in={collapse.wineType} component="ul">
				{nationalities?.map(nation => <li className='unSelected-paragraph'>{nation.name}</li>)}
			</Collapse>
			
	  </div>
	  <Container className="overflow-auto wine-container" maxWidth="lg">
		<Grid container spacing={4} xs={12} sm={12} md={12} lg={12} className="pt-16">
		  {categories?.map((item) => (
			<Grid item xs={12} sm={6} md={6} lg={4} >
			  <Card elevation={0}>
				<CardContent className="position-relative h-400">
				  <span>{item.name}</span>
				  <span>{item.price}</span>
				  <div
					className="position-absolute card-bg"
					style={{
					  backgroundImage: `url(${imagesCategory[item.urls[0]]})`,
					}}
				  ></div>
				</CardContent>
			  </Card>
			</Grid>
		  ))}
		</Grid>
	  </Container>
	</div>
  
  )
}

export default Category

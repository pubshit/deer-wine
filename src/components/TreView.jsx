import React, { useState } from 'react';

// TreeNode component
const TreeNode = ({ node }) => {
  const [isExpanded, setIsExpanded] = useState(false);
  
  const handleToggle = () => {
	setIsExpanded(!isExpanded);
  };
  
  return (
	<div>
	  <div onClick={handleToggle}>
		{node.children && (isExpanded ? 'v ' : '> ')}
		{node.name}
	  </div>
	  {isExpanded && node.children && (
		<div style={{ marginLeft: 20 }}>
		  {node.children.map((childNode) => (
			<TreeNode key={childNode.id} node={childNode} />
		  ))}
		</div>
	  )}
	</div>
  );
};

// TreeView component
const TreeView = ({ data }) => {
  return (
	<div>
	  {data.map((node) => (
		<TreeNode key={node.id} node={node} />
	  ))}
	</div>
  );
};

export default TreView
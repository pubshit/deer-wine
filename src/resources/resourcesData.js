

const imagesSlide = {};
function importAll() {
    let introImages = require.context('../../public/assets/images/intro', false, /\.(png|jpe?g|svg)$/)
    introImages.keys().forEach((key) => (imagesSlide[key] = introImages(key)));
}
importAll();


export const wineSlide = [
    {
        id: 1,
        name: "THE WORLD OF WINE",
        description: "ABOUT 600-800 GRAPES GO INTO MAKING JUST ONE BOTTLE OF WINE",
        url:imagesSlide['./slide-1.jpg']
    },
    {
        id: 2,
        name: "NOT SNACKING GRAPE",
        description: "Around 90% of cultivated wine grapes worldwide are ‘Vitis vinifera’.",
        url:imagesSlide['./grape.jpg']
    },
    {
        id: 3,
        name: "C O M P L E X I T Y",
        description: "What makes wine complex – " +
            "and extremely enjoyable – " +
            "is the potential to influence the winemaking process in many ways," +
            " and this is what gives us all different kinds and styles of wine.",
        url:imagesSlide['./intro-2.jpg']
    },
    {
        id: 4,
        name: "AGEING AS ART",
        description: "The art of aging in winemaking is a special process that affects " +
            "the taste and aroma of wine what takes time and skill to produce " +
            "a high-quality product.",
        url:imagesSlide['./ageing-slide-3.jpg']
    },
    {
        id: 5,
        name: "WINE'S COLO U R",
        description: "Unmistakably, a bright wine is a young, light-colored wine, such a Semillon from Australia's Hunter Valley, that is typically pale and unoaked.\n",
        url:imagesSlide['./slide-3.jpg']
    },
    {
        id: 6,
        name: "T A S T E",
        description: "It's time to explore the wine's flavors and structure. " +
            "We'll find out how strong the alcohol and acidity are, " +
            "how much tannin is present, and how the wine finishes.",
        url:imagesSlide['./taste.jpg']
    },
]

export const categories = [
    {
        name: 'Aalto',
        price: 53.83,
        quantity: 58,
        type: 0,
        madeBy: 0,
        urls: ['./aalto-4.jpg', './aalto-1.jpg', './aalto-2.jpg', './aalto-3.jpg', './aalto.jpg'],
    },
    {
        name: 'Acroterra Santorini',
        price: 95.00,
        quantity: 37,
        type: 1,
        madeBy: 4,
        urls: ['./acroterra-santorini-2.jpg', './acroterra-santorini-1.jpg', './acroterra-santorini-3.jpg', './acroterra-santorini-1.jpg'],
    },
    {
        name: 'Apogeu',
        price: 12.61,
        quantity: 68,
        type: 1,
        madeBy: 5,
        urls: ['./apogeu-3.jpg', './apogeu-1.jpg', './apogeu-2.jpg', './apogeu.jpg'],
    },
    {
        name: 'Pauillac (Grand Cru Classé)',
        price: 122.99,
        quantity: 23,
        type: 2,
        madeBy: 3,
        urls: ['./batailley-1.jpg', './batailley-2.jpg', './batailley-3.jpg'],
    },
    {
        name: 'Black',
        price: 19.19,
        quantity: 36,
        type: 1,
        madeBy: 1,
        urls: ['./black-3.jpg', './black-2.jpg', './black-1.jpg', './black-4.jpg'],
    },
    {
        name: 'Cabernet Sauvignon',
        price: 11.24,
        quantity: 24,
        type: 1,
        madeBy: 5,
        urls: ['./hol-don-3.jpg', './hol-don-2.jpg', './hol-don-1.jpg'],
    },
    {
        name: 'LIEBFRAUMILCH - KRAEMER',
        price: 11.99,
        quantity: 12,
        type: 1,
        madeBy: 2,
        urls: ['./liebfrauenmilch-4.jpg', './liebfrauenmilch-1.jpg', './liebfrauenmilch-2.jpg', ],
    },
    {
        name: 'pinot noir 2017',
        price: 37.05,
        quantity: 27,
        type: 2,
        madeBy: 4,
        urls: ['./novak-pinot-noir-2.jpg','./novak-pinot-noir.jpg','./novak-pinot-noir.jpg-1',],
    },
    {
        name: 'Stina Posip',
        price: 22.22,
        quantity: 57,
        type: 2,
        madeBy: 3,
        urls: ['./stina-1.jpg','./stina.jpg','./stina1.jpg','./stina2.jpg',],
    },
    {
        name: 'Agiorgitiko',
        price: 17.29,
        quantity: 57,
        type: 0,
        madeBy: 2,
        urls: ['./titana-6.jpg','./titana.jpg','./titana-5.jpg',],
    },
]

export const nationalities = [
    {
        id: 0,
        code: 'FRANCE',
        name: 'France',
    },
    {
        id: 1,
        code: 'AUSTRALIA',
        name: 'Australia',
    },
    {
        id: 2,
        code: 'CHILE',
        name: 'Chile',
    },
    {
        id: 3,
        code: 'ITALY',
        name: 'Italy',
    },
    {
        id: 4,
        code: 'SPAIN',
        name: 'Spain',
    },
    {
        id: 5,
        code: 'GERMANY',
        name: 'Germany',
    },
]

export const wineType = [
    {
        id: 0,
        name: 'Red Wine',
    },
    {
        id: 1,
        name: 'White Wine',
    },
    {
        id: 2,
        name: 'Black Wine',
    },
]